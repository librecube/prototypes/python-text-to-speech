from text_to_speech import TextToSpeech


tts = TextToSpeech()

try:
    while True:
        tts.speak("Hello world! This is a text to be read out aloud. Hope it works.")
        input("Hit <Enter> to repeat")
except KeyboardInterrupt:
    pass
