# Python Text to Speech

To be written...

## Installation

Install via pip:

```
pip install git+https://gitlab.com/librecube/prototypes/python-text-to-speech
```

## Example

To be written...

## Contribute

- Issue Tracker: https://gitlab.com/librecube/prototypes/python-text-to-speech/-/issues
- Source Code: https://gitlab.com/librecube/prototypes/python-text-to-speech

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube documentation](https://librecube.gitlab.io/).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
