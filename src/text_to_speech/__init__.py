import torch
from transformers import SpeechT5Processor, SpeechT5ForTextToSpeech
from transformers import SpeechT5HifiGan
from datasets import load_dataset


processor = SpeechT5Processor.from_pretrained("microsoft/speecht5_tts")
model = SpeechT5ForTextToSpeech.from_pretrained("microsoft/speecht5_tts")

embeddings_dataset = load_dataset("Matthijs/cmu-arctic-xvectors", split="validation")
#embeddings_dataset = load_dataset("parquet", data_files="cmu-arctic-xvectors-validation.parquet", split="train")

vocoder = SpeechT5HifiGan.from_pretrained("microsoft/speecht5_hifigan")


class TextToSpeech:

    def __init__(self):
        pass

    def speak(self, text):
        import random
        speaker = random.randint(1, 7899)
        speaker_embeddings = torch.tensor(embeddings_dataset[speaker]["xvector"]).unsqueeze(0)
        inputs = processor(text=text, return_tensors="pt")
        speech = model.generate_speech(inputs["input_ids"], speaker_embeddings, vocoder=vocoder)

        import sounddevice as sd
        sd.play(speech.numpy(), 16000)
        sd.wait()

        # import espeakng
        # mySpeaker = espeakng.Speaker()
        # mySpeaker.say(text)
